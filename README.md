# Internet of Trains
Our idea is to show detailed information compressed in a single view about a railway station you choose. We provide different data sets like weather and further datas are following.

## TrainNet - iOS Application
This is the offical iOS app for the project *Internet of Trains* and also contains a framework for using in another project. To use this app install Xcode 7 with Swift 2.2.1 and run it in the simulator or on a device. 

### Framework
You can use the framework for implementing it in your own projects and building your very own great application for showing railway station data.

For downloading and storing a list of all railway station just call the method `TNStationAPI.downloadAllStations()`. To search for a station `TNStationAutocompleteObject.stationsFor("Berlin")` returns a list of all that contains the searched string. By calling `currentStation?.requestData(_:TNDataSet)` it will request all available data call the block you pass in it.

### Unit Tests
The workspace contains some unit tests for the framework for example to test if searching for a station works and will not return all.


_____
The app contains code from Alamofire Software Foundation, Ramotion Inc., Iftekhar Qurashi and Eddy Borja.

The code is provided to you under the MIT licence that is append to the repository.