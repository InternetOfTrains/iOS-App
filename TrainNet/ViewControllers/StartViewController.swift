//
//  StartViewController.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import MLPAutoCompleteTextField
import TrainNetKit

class StartViewController: UIViewController {

    //MARK: Actions
    @IBAction func next() {
        if currentStation?.dataSet == nil {
            isLoading = true
            currentStation?.requestData({
                dataSet in
                self.goToNext()
                self.isLoading = false
            })
        }
        else {
            goToNext()
        }
    }
    
    //MARK: Outlets
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: Attributes
    private var currentStation:TNStation? {
        didSet {
            reloadButtonState()
        }
    }
    
    private var isLoading = false {
        didSet {
            reloadButtonState()
            if isLoading {
                activityIndicator.startAnimating()
            }
            else {
                activityIndicator.stopAnimating()
            }
        }
    }
    
    //MARK: Main
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func reloadButtonState() {
        button.enabled = currentStation != nil && !isLoading
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let navController = segue.destinationViewController as? UINavigationController,  controller = navController.viewControllers.first as? DetailsTableViewController {
            controller.station = currentStation
        }
    }
    
    private func goToNext() {
        self.performSegueWithIdentifier("go", sender: nil)
    }
}

extension StartViewController : MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource {
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, possibleCompletionsForString string: String!) -> [AnyObject]! {
        return TNStationAutocompleteObject.stationsFor(string)
    }
    
    func autoCompleteTextField(textField: MLPAutoCompleteTextField!, didSelectAutoCompleteString selectedString: String!, withAutoCompleteObject selectedObject: MLPAutoCompletionObject!, forRowAtIndexPath indexPath: NSIndexPath!) {
        if let object = selectedObject as? TNStationAutocompleteObject {
            currentStation = object.station
        }
    }
}
