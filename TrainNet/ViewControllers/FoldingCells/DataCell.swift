//
//  DataCell.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import FoldingCell
import TrainNetKit

class DataCell: FoldingCell {

    var weather: TNWeather? {
        didSet {
            let imageData = NSData(contentsOfURL: NSURL(string: "http://openweathermap.org/img/w/\(weather!.icon).png")!)!
            let image = UIImage(data: imageData)
            setImage(image)
            weatherDescription.text = weather?.description
            setTitleText(NSLocalizedString("Weather", comment: "Weather title"))
            
        }
    }
    
    var accessibility: TNAccessibility? {
        didSet {
            let image = UIImage(named: "Elevator")
            setImage(image)
            let elevatorState = accessibility!.active ? NSLocalizedString("Active", comment: "Elevator state") : NSLocalizedString("Deactive", comment: "Elevator state")
            setTitleText(String.localizedStringWithFormat(NSLocalizedString("Elevator state: %@", comment: "Elevator state description"), elevatorState))
            weatherDescription.text = accessibility?.description
        }
    }
    
    @IBOutlet weak var titleOneLabel: UILabel!
    @IBOutlet weak var titleTwoLabel: UILabel!
    @IBOutlet weak var iconOneImageView: UIImageView!
    @IBOutlet weak var iconTwoImageView: UIImageView!
    @IBOutlet weak var weatherDescription: UILabel!
    
    override func animationDuration(itemIndex:NSInteger, type:AnimationType)-> NSTimeInterval {
        let durations = [0.26, 0.2, 0.2]
        return durations[itemIndex]
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.foregroundView.backgroundColor = TNColor.red()
        self.containerView.backgroundColor = TNColor.darkRed()
    }
    
    private func setTitleText(title:String) {
        titleOneLabel.text = title
        titleTwoLabel.text = title
    }
    
    private func setImage(image:UIImage?) {
        iconOneImageView.image = image
        iconTwoImageView.image = image
    }
    
}
