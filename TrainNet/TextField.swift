//
//  TextField.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit
import TrainNetKit
import MLPAutoCompleteTextField

class TextField: MLPAutoCompleteTextField {

    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        self.layer.borderColor = TNColor.red().CGColor
        self.layer.borderWidth = 3
    }

    let padding = UIEdgeInsets(top: 2, left: 7, bottom: 2, right: 7);
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
}
