//
//  FlatButton.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import UIKit

@IBDesignable class FlatButton: UIButton {
    
    @IBInspectable var background: UIColor = UIColor.clearColor() {
        didSet {
            self.backgroundColor = background
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 10 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable var marginsHorizontal: CGFloat = 5 {
        didSet {
            updateInsets()
        }
    }
    
    @IBInspectable var marginsVertical: CGFloat = 5 {
        didSet {
            updateInsets()
        }
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.backgroundColor = background
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        updateInsets()
    }
    
    private func updateInsets() {
        self.titleEdgeInsets = UIEdgeInsetsMake(marginsHorizontal, marginsVertical, marginsHorizontal, marginsVertical)
        self.contentEdgeInsets = UIEdgeInsetsMake(marginsHorizontal, marginsVertical, marginsHorizontal, marginsVertical)
    }
    
    override func intrinsicContentSize() -> CGSize {
        let s = super.intrinsicContentSize()
        
        return CGSizeMake(s.width + (self.titleEdgeInsets.left * 2), s.height + (2 * self.titleEdgeInsets.top))
    }
    
}
