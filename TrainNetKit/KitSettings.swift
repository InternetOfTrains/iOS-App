//
//  KitSettings.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation

/// Stores the data manager for CoreData
internal var TNKCoreDataManager = CoreDataManager()
/// Stores the CoreData managed object context for other operations
internal var TNKContext = TNKCoreDataManager.managedObjectContext!