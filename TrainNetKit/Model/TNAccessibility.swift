//
//  TNAccessibility.swift
//  TrainNet
//
//  Created by Alex on 12/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation

/// Represents a accessibility object that contains a elevator of a station with a description and the current state.
public class TNAccessibility {
        /// Short description of the elevator
    public let description:String
        /// State if the elevator is active
    public let active:Bool
    
    /**
     Inizalizes a new object.
     
     - parameter description: Short description of the elevator
     - parameter active:      State if the elevator is active
     
     - returns: A new object
     */
    init(description:String, active:Bool) {
        self.description = description
        self.active = active
    }
}