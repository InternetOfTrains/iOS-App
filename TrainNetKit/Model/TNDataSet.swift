//
//  TNDataSet.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation

/// Represents a data set object that will hold all data that can be available for a station.
public class TNDataSet {
     /// Weather data
    public var weather:TNWeather?
     /// Accessibility data
    public var accessibility:TNAccessibility?
}