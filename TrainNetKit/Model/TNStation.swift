//
//  TNStation.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import CoreData

/// Represents the object of a station and is a managed object for the CoreData entity.
public class TNStation : NSManagedObject {
        /// Internal station code of the station
    @NSManaged public var stationCode:String
    
        /// Railway station id for requesting further information from DB api
    @NSManaged public var railwayStationId:String
        /// Title of the station
    @NSManaged public var title:String
        /// Longitude of the station
    @NSManaged public var long:Double
        /// Latitue of the station
    @NSManaged public var lat:Double
    
        /// Data set of the railway station
    public var dataSet:TNDataSet?
    
    override init(entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        super.init(entity: entity, insertIntoManagedObjectContext: context)
    }
    
    /**
     Initalize a new station object with a given title and saves it to the CoreData.
     
     - parameter stationCode: Internal station code of the station
     - parameter railwayStationId: Station id for requesting further information from DB api
     - parameter title: Title of the railway station
     - parameter long: Longitute of the station
     - parameter lat: Latitue of the station
     
     - returns: A new station object
     */
    init(stationCode:String, railwayStationId:String, title:String, long:Double, lat:Double) {
        super.init(entity: NSEntityDescription.entityForName("Station", inManagedObjectContext: TNKContext)!, insertIntoManagedObjectContext: TNKContext)
        self.stationCode = stationCode
        self.railwayStationId = railwayStationId
        self.title = title
        self.long = long
        self.lat = lat
        
        TNKCoreDataManager.saveContext()
    }
    
    /**
     Searchs for stations in CoreData with a given predicate.
     
     - parameter predicate: Predicate for searching
     
     - returns: Array of all station that match the predicate
     */
    public class func searchStationsFor(predicate:NSPredicate?)->Array<TNStation> {
        let fetch = NSFetchRequest()
        fetch.entity = NSEntityDescription.entityForName("Station", inManagedObjectContext: TNKContext)
        fetch.returnsObjectsAsFaults = false
        fetch.predicate = predicate
        
        return (try! TNKContext.executeFetchRequest(fetch)) as? Array<TNStation> ?? []
    }
    
    /**
     Returns if a station with the given title already exists in CoreData.
     
     - parameter title: Title of the station that should be searched for
     
     - returns: Boolean if the station already exists
     */
    class func doesStationExist(title:String)->Bool {
        let stations = searchStationsFor(NSPredicate(format: "title == %@", argumentArray: [title]))
        return stations.count == 1
    }
    
    /**
     Request all data set for a station.
     
     - parameter completion: Block that will be executed when request completed
     */
    public func requestData(completion:(dataSet: TNDataSet)->Void) {
        let dataSet = TNDataSet()
        TNStationAPI.getWeatherInformation(forStation: self, completion: {
            weather in
            dataSet.weather = weather
            TNStationAPI.getAccessibility(forStation: self, completion: {
                accessibility in
                dataSet.accessibility = accessibility
                self.dataSet = dataSet
                completion(dataSet: dataSet)
            })
        })
    }
    
}
