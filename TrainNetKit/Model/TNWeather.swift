//
//  TNWeather.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation

/// Represents a weather object.
public class TNWeather {
        /// Detailed description of the weather situation
    public let description:String
        /// Icon name for the weather situation
    public let icon:String
    
    /**
     Inizalizes a new weather object.
     
     - parameter description: Weather description
     - parameter icon:        Icon string
     
     - returns: Weather object
     */
    init(description:String, icon:String) {
        self.description = description
        self.icon = icon
    }
}