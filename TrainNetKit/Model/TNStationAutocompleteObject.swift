//
//  TNStationAutocompleteObject.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import MLPAutoCompleteTextField

/// This is a wrapper class for the autocompletion process around a station. Furthermore it contains a method for search for a railway station.
public class TNStationAutocompleteObject : NSObject, MLPAutoCompletionObject {
        /// Station object of wrapper class
    public let station:TNStation
    
    /**
     Inizalize a new station wrapper object.
     
     - parameter station: A given station
     
     - returns: A new wrapper object
     */
    public init(station:TNStation) {
        self.station = station
    }
    
    /**
     Returns the string that is shown in the autocompletion process.
     
     - returns: Title of the station
     */
    @objc public func autocompleteString() -> String! {
        return station.title
    }
    
    /**
     Search in the list of all saved railway stations and returns an array with all stations in wrapper objects.
     
     - parameter text: Text that should be contained in the titles of the stations
     
     - returns: Array of wrapper objects around founded stations
     */
    public class func stationsFor(text: String)->Array<TNStationAutocompleteObject> {
        let predicate = NSPredicate(format: "title CONTAINS[cd] %@", argumentArray: [text])
        let allStations = TNStation.searchStationsFor(predicate)
        
        var stationObjects = Array<TNStationAutocompleteObject>()
        
        for station in allStations {
            stationObjects.append(TNStationAutocompleteObject(station: station))
        }
        return stationObjects
    }
}