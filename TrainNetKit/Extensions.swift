//
//  Extensions.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation

public extension UIColor {
    /**
     Initalizes a color by given RGB values
     
     - parameter red:   Red value between 0 and 255.
     - parameter green: Green value between 0 and 255.
     - parameter blue:  Blue value between 0 and 255.
     
     - returns: A color by RGB values.
     */
    public convenience init(red:Double, green:Double, blue:Double) {
        self.init(red: CGFloat(red / 255), green: CGFloat(green / 255), blue: CGFloat(blue / 255), alpha: 1)
    }
}