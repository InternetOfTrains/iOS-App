//
//  TNColor.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation

public class TNColor {
    public class func red()->UIColor {
        return UIColor(red: 239, green: 83, blue: 80)
    }
    
    public class func darkRed()->UIColor {
        return UIColor(red: 183, green: 28, blue: 28)
    }
}