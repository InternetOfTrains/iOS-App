//
//  TNStationAPI.swift
//  TrainNet
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import Alamofire

/// Represents a wrapper around the server REST-API and contains all possible methods.
public class TNStationAPI {
    /**
     Stores the base url of the REST-API
     */
    private static let baseURL = "https://iot1jhwest.herokuapp.com"
    
    /**
     Starts downloading all railway stations from the server and saving it to core data
     
     - parameter completion: Block that will be executed when stations has been downloaded
     */
    public class func downloadAllStations(completion:()->Void) {
        guard !hasDownloaded() else { return }
        
        Alamofire.request(.GET, baseURL+"/api/backend/railways/get/nodes").responseJSON() {
            response in
            if let json = response.result.value as? Dictionary<String, AnyObject>, stations = json["stations"] as? Array<Dictionary<String, AnyObject>> where response.response?.statusCode == 200 {
                for station in stations {
                    if let title = station["name"] as? String, geometry = station["geometry"] as? Dictionary<String, Double>, long = geometry["long"], lat = geometry["lat"], property = station["property"], stationCode = property["StationCode"] as? String, stationId = property["StationID"] as? String where !TNStation.doesStationExist(title) {
                        let _ = TNStation(stationCode: stationCode, railwayStationId: stationId, title: title, long: long, lat: lat)
                    }
                }
                downloaded()
                completion()
            }
        }
    }
    
    /**
     Requests the weather information for a railway station from the server.
     
     - parameter station:    A railway station you want to search the weather for.
     - parameter completion: Block that has the requested weather object.
     */
    public class func getWeatherInformation(forStation station:TNStation, completion:(weather:TNWeather?)->Void) {
        Alamofire.request(.POST, baseURL+"/api/backend/weatcher", parameters: ["lat": station.lat, "long": station.long], encoding: .JSON, headers: nil).responseJSON() {
            response in
            if let json = response.result.value as? Dictionary<String, AnyObject>, weathers = json["weather"] as? Array<Dictionary<String, AnyObject>>, weather = weathers.first, description = weather["description"] as? String, icon = weather["icon"] as? String where response.response?.statusCode == 200 {
                let weather = TNWeather(description: description, icon: icon)
                completion(weather: weather)
            }
            else {
                completion(weather: nil)
            }
        }
    }
    
    /**
     Requests accessiblity inforation for a given station from the DB api server.
     
     - parameter station:    A railway station you want to search for.
     - parameter completion: Block that has the requested weather object.
     */
    public class func getAccessibility(forStation station:TNStation, completion:(accessibility:TNAccessibility?)->Void) {
        Alamofire.request(.GET, "https://adam.noncd.db.de/api/v1.0/stations/"+station.railwayStationId).responseJSON() {
            response in
            if let json = response.result.value as? Dictionary<String, AnyObject>, facilities = json["facilities"] as? Array<Dictionary<String, AnyObject>>, facility = facilities.first, type = facility["type"] as? String, state = facility["state"] as? String, description = facility["description"] as? String where type == "ELEVATOR" {
                let accessibility = TNAccessibility(description: description, active: state == "ACTIVE")
                completion(accessibility: accessibility)
            }
            else {
                let assessibility = TNAccessibility(description: "Not available", active: false)
                completion(accessibility: assessibility)
            }
        }
    }
    
    /**
     Returns if all stations have beed downloaded.
     
     - returns: Value if stations have beed downloaded
     */
    public class func hasDownloaded()->Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.boolForKey("Downloaded")
    }
    
    /**
     Sets setting that all stations have beed downloaded.
     */
    private class func downloaded() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: "Downloaded")
        defaults.synchronize()
    }
}