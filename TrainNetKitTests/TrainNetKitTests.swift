//
//  TrainNetKitTests.swift
//  TrainNetKitTests
//
//  Created by Alex on 11/06/16.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import XCTest
@testable import TrainNetKit

class TrainNetKitTests: XCTestCase {

    /**
     Tests if there are stations in CoreData
     */
    func testListOfStations() {
        let stations = TNStation.searchStationsFor(nil)
        XCTAssert(stations.count > 0)
    }
    
    /**
     Tests if when searching for a station it returns only one result
     */
    func testSearchingStations() {
        let stations = TNStation.searchStationsFor(NSPredicate(format: "title == %@", "Köln Messe/Deutz"))
        XCTAssert(stations.count == 1)
    }
}
